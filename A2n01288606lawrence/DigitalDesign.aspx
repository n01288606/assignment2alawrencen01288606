﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DigitalDesign.aspx.cs" Inherits="A2n01288606lawrence.DigitalDesign" %>
<asp:Content ID="trickyConcept" ContentPlaceHolderID="MainContent" runat="server">
    <p>Positioning in CSS can be rather tricky. Static Positioning is the default position, and the elements with this positioning cannot use top, left, right, or bottom properties. The elements, using this position, show up in the location determined by the tag they are held in, their "natural location". Relative positioning also starts the element out in their "natural locations". The element can use top, left, right, or bottom properties. Absolute positioning is different because the element with absolute positioning is taken out of the flow of elements and is positioned relative to the closet parent with non-static positioning.  Fixed Positioned elements are also taken out of the flow, but is also positioned relative to the window instead of a parent element. </p>
</asp:Content>
<asp:Content ID="myCode" ContentPlaceHolderID="SideBarFirst" runat="server">
   <p> .main-navigation li{
    display:inline-block;
    margin-right:1em;
    font-size:1.5em;
    font-weight: bold;
}</p>
    <p>This code styles my navigation list on the web page I made for Digital Design.  The display inline-block makes the list items go in a single line. The margin gives each list item more space on the right side. Font-size and font-weight size the font and emboldens the text.</p>
</asp:Content>
<asp:Content ID="onlineCode" ContentPlaceHolderID="SideBarSecond" runat="server">
    <p>-style-
h1 {
    text-align: center;
}

h2 {
    text-align: left;
}

h3 {
    text-align: right;
}
-/style-
-/head-
-body-

-h1-Heading 1 (center)-/h1-
-h2-Heading 2 (left)-/h2-
-h3-Heading 3 (right)-/h3-</p>
    <p>In this code, from W3 Schools Text-align page, I changed the <> with dashed to prevent it from running. This code has three different headings, and the text-align properties position the content  within the space alloted to the heading tags. </p>
</asp:Content>
<asp:Content ID="myLinks" ContentPlaceHolderID="LinksContent" runat="server">
    <p><a href="https://www.w3schools.com/css/css_positioning.asp" title="Click here to visit W3 Schools Properties Page">Click here </a>to visit W3 Schools Properties Page</p>
    <p><a href="https://developer.mozilla.org/en-US/docs/Web/CSS/position" title="Here's Mozilla Developer's Position Page">Mozilla Developer"s </a>Position Page provides a lot of information on the different positionings, and their syntax.</p>
    <p><a href="http://web.simmons.edu/~grabiner/comm244/weekthree/css-basic-properties.html" title="Simmons University's basic properties page">Simmons University </a> made a list of CSS properties!</p>
</asp:Content>