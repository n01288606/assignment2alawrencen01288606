﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SQL.aspx.cs" Inherits="A2n01288606lawrence.SQL" %>
<asp:Content ID="trickyConcept" ContentPlaceHolderID="MainContent" runat="server">
    <p>SQL Joins can be a tricky concept.  A Join is when you are slecting from multiple tables with one query. Each table has to have a primary key,  and one of the tables needs a foreign key to link the two tables. There are multiple joins, and the best way to think about them is to picture a Venn Diagram. An inner join pulls from the overlapping middle section. A left join pulls from the left side and the inner section, and a right join pulls from the right side and inner section.  AN outer joing ignores the inner section, and only pulls from the outer sides. These tables use a one to many relationship, an example of this relationship is that one person can't be in two cars but one car can have multiple people. The joinds become more advanced when they use a many to many relationship. One library has many books and one book can have copies in many libraries.  If this is the case then two tables will need a bridging table. Each table, including the bridging table, will have primary keys, and the bridging table will have the foreign keys from the other two tables. The query will connect the first table to the bridge table and then the second table to bridge table.  </p>
</asp:Content>
<asp:Content ID="myCode" ContentPlaceHolderID="SideBarFirst" runat="server">
    <p>Select clients.clientid, client.fname, clients.clientlname, count(cars.carid) as client_cars from cars inner join carsxclients on cars.carid = carsxclients.carid inner join clients on clients.clientid = carsxclients.clientid group by clients.clientid, clients.clientfname, clients.clientlname;<p>
        <p>This is from Assignment 3 and I selected the clients and how many cars belonged to each client.  The client information was selected from clients table which was bridged to the carxclients table. The cars number was pulled from the cars table which was also bridged to the carsxclients table. THe aggregate count was used to determine the number of cars per client.</p>
</asp:Content>
<asp:Content ID="onlineCode" ContentPlaceHolderID="SideBarSecond" runat="server">
    <p>SELECT table1.column1,table1.column2,table2.column1,....
FROM table1 
LEFT JOIN table2
ON table1.matching_column = table2.matching_column;

table1: First table.
table2: Second table
matching_column: Column common to both the tables.</p>
    <p>This table from <a href="https://www.geeksforgeeks.org/sql-join-set-1-inner-left-right-and-full-joins/">geeksforgeeks.org</a>explains how to do a Left join. You take the table 1 and join on table 2.  </p>
</asp:Content>
<asp:Content ID="myLinks" ContentPlaceHolderID="LinksContent" runat="server">
   <p><a href="https://www.w3schools.com/sql/sql_join.asp" title="Click here for SQL W3 Schools">Click Here </a> for the W3 school's SQL joins page.</p>
    <p><a href="http://www.sql-join.com/sql-join-types/" title="This site explains SQL Joins">sql-join.com </a> explains what joins are as well as the types and provides other resources.</p>
    <p><a href="https://www.essentialsql.com/how-to-write-queries-step-1/" title="Click here for essential sql">essentialsql.com </a>provides an explanation of how to write an sql query and explains how to use joins.</p>
</asp:Content>