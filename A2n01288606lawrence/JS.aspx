﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JS.aspx.cs" Inherits="A2n01288606lawrence.JS" %>

<asp:Content ID="trickyConcept" ContentPlaceHolderID="MainContent" runat="server">
    <p>I"m not sure if functions would qualifiy as a "tricky concept", but I certainly find them tricky. To create a function you first use the keyword "function", then you name it, and then add parenthesis followed by curly braces. The parenthesis will hold the parameters. Inside the curly braces will be the variables and other statesment, such as if statements. The following sets of curly braces could hold the previous variable assigned to a new value, and the return statement.</p>

</asp:Content>
<asp:Content ID="myCode" ContentPlaceHolderID="SideBarFirst" runat="server">
    <p>
        makeWithDrawal: function() {
        var withDrawal = prompt("How Much Are You Withdrawing Today?");
        withDrawal = parseInt(withDrawal);
        bankCustomer.accountBalance = bankCustomer.accountBalance - withDrawal;
        alert("Thank You, your current balance is now $" + bankCustomer.accountBalance.toFixed(2));
    </p>
    <p>I wrote this with a partner in Web Programming! It is a function that is designed to mimic a bank withdrawal.  The variable var withDrawal uses a prompt to ask the amount that will be taken out.  The entered amount is switched to the integer using parseint, and then the amount is subtracted by the bank total stated in a variable outside the function.  Then an alert comes up with a string concatenated with the current account balance.</p>
</asp:Content>
<asp:Content ID="onlineCode" ContentPlaceHolderID="SideBarSecond" runat="server">
    <p>I found this Function on W3 Schools.  It's simple, but it's a function that is designed to calculate the conversion between celsius and fahrenheit. THe variable fahrenheit is an integer that has 32 subtracted from it and is multiplied by five divided by 9.  </p>
    <p>
        function toCelsius(fahrenheit) {
    return (5/9) * (fahrenheit-32);
}   
    </p>
</asp:Content>
<asp:Content ID="myLinks" ContentPlaceHolderID="LinksContent" runat="server">
    <p><a href="https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/JavaScript_basics" title="Click to see Mozilla Developer">developer.mozilla.org </a> helps explain functions as well as other introductory concepts.</p>
    <p><a href="https://javascript.info/" title="Click to see JavaScript.info">JavaScript.info </a>provides pages of various JavaScript concepts </p>
    <p><a href="https://www.w3schools.com/js/js_functions.asp" title="Click to visit W3 Schools Functions Page">Click Here </a> to visit W3 School's Functions Page</p>
</asp:Content>
